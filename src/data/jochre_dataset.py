import os
import math
import torch
import torchvision.transforms as transforms
from PIL import Image, ImageOps
from torch.utils.data import Dataset
from torchvision.transforms import functional

class FixedHeightResize:
    def __init__(self, size):
        self.size = size

    def __call__(self, img):
        w, h = img.size
        aspect_ratio = float(h) / float(w)
        new_w = math.ceil(self.size / aspect_ratio)
        return functional.resize(img, (self.size, new_w))

class JochreDataset(Dataset):
    def __init__(self, opt):
        super(JochreDataset, self).__init__()
        self.path = os.path.join(opt.path, opt.imgdir)
        self.images = os.listdir(self.path)
        self.nSamples = len(self.images)
        f = lambda x: os.path.join(self.path, x)
        self.imagepaths = list(map(f, self.images))
       	transform_list =  [transforms.Grayscale(1),
                           FixedHeightResize(opt.imgH),
                            transforms.ToTensor(), 
                            transforms.Normalize((0.5,), (0.5,))]
        self.transform = transforms.Compose(transform_list)
        labelpath = os.path.join(opt.path, opt.labeldir, 'word-to-text.txt')
        with open(labelpath) as fin:
            rows = ( line.split('\t') for line in fin )
            self.labels = { row[0]:row[1] for row in rows }
        # self.collate_fn = SynthCollator()

    def __len__(self):
        return self.nSamples

    def __getitem__(self, index):
        assert index <= len(self), 'index range error'
        imagepath = self.imagepaths[index]
        imagefile = os.path.basename(imagepath)
        img = Image.open(imagepath)
        #img = ImageOps.contain(img, (100,opt.imgH))
        if self.transform is not None:
            img = self.transform(img)
        item = {'img': img, 'idx':index}
        item['label'] = self.labels[imagefile]
        return item 

class JochreCollator(object):
    
    def __call__(self, batch):

        # img_path = [item['img_path'] for item in batch]
        width = [item['img'].shape[2] for item in batch]
        indexes = [item['idx'] for item in batch]
        imgs = torch.ones([len(batch), batch[0]['img'].shape[0], batch[0]['img'].shape[1], max(width)], dtype=torch.float32)
        for idx, item in enumerate(batch):
            try:
                imgs[idx, :, :, 0:item['img'].shape[2]] = item['img']
            except:
                print(imgs.shape)
        item = {'img': imgs, 'idx':indexes}
        if 'label' in batch[0].keys():
            labels = [item['label'] for item in batch]
            item['label'] = labels
        return item

