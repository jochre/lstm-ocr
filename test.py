import warnings
from argparse import ArgumentParser

import matplotlib.pyplot as plt
from torch.utils.data.sampler import SubsetRandomSampler
from tqdm import *

from src.data.jochre_dataset import JochreDataset, JochreCollator
from src.models.crnn import CRNN
from src.options.opts import base_opts
from src.utils.utils import *

warnings.filterwarnings("ignore")
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
torch.manual_seed(0)
np.random.seed(0)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

def get_accuracy(args):
    loader = torch.utils.data.DataLoader(args.data,
                batch_size=args.batch_size,
                collate_fn=args.collate_fn)
    model = args.model
    model.eval()
    converter = OCRLabelConverter(args.alphabet)
    evaluator = Eval()
    labels, predictions = [], []
    images = []
    for iteration, batch in enumerate(tqdm(loader)):
        input_, targets = batch['img'].to(device), batch['label']
        labels.extend(targets)
        images.extend(input_.squeeze().detach())
        targets, lengths = converter.encode(targets)
        logits = model(input_).transpose(1, 0)
        logits = torch.nn.functional.log_softmax(logits, 2)
        logits = logits.contiguous().cpu()
        T, B, H = logits.size()
        pred_sizes = torch.LongTensor([T for i in range(B)])
        probs, pos = logits.max(2)
        pos = pos.transpose(1, 0).contiguous().view(-1)
        sim_preds = converter.decode(pos.data, pred_sizes.data, raw=False)
        predictions.extend(sim_preds)

    if not args.cuda:
        fig=plt.figure(figsize=(16, 8))
        columns = 4
        rows = 10
        for i in range(1, columns*rows +1):
            img = images[i]
            img = (img - img.min())/(img.max() - img.min())
            img = np.array(img * 255.0, dtype=np.uint8)
            fig.add_subplot(rows, columns, i)
            plt.title(predictions[i])
            plt.axis('off')
            plt.imshow(img)
        plt.show()

    ca = np.mean((list(map(evaluator.char_accuracy, list(zip(predictions, labels))))))
    wa = np.nanmean((list(map(evaluator.word_accuracy_line, list(zip(predictions, labels))))))
    return ca, wa


def main(**kwargs):
    parser = ArgumentParser()
    base_opts(parser)
    args = parser.parse_args()
    args.data = JochreDataset(args)
    args.collate_fn = JochreCollator()
    args.alphabet = """!()*,./0123456789:;<>?CL[]_־אאַאָבבּבֿגדהווּזחטייִךככּכֿלםמןנסעףפפּפֿץצקרששׂתתּװױײײַ—’“„•⁄"""
    args.nClasses = len(combinedchars(args.alphabet))
    model = CRNN(args)
    args.cuda = torch.cuda.is_available()
    if args.cuda:
        model = model.cuda()
    resume_file = os.path.join(args.save_dir, args.name, 'best.ckpt')
    if os.path.isfile(resume_file):
        print('Loading model %s'%resume_file)
        if args.cuda:
            checkpoint = torch.load(resume_file)
        else:
            checkpoint = torch.load(resume_file, map_location=torch.device('cpu'))
        model.load_state_dict(checkpoint['state_dict'])
        args.model = model
        ca, wa = get_accuracy(args)
        print("Character Accuracy: %.2f\nWord Accuracy: %.2f"%(ca, wa))
    else:
        print("=> no checkpoint found at '{}'".format(resume_file))
        print('Exiting')

if __name__ == '__main__':
    main()